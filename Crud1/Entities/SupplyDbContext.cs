﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crud1.Entities
{
    public class SupplyDbContext : DbContext
    {
        public SupplyDbContext(DbContextOptions<SupplyDbContext> options) : base(options)
        {
        }

        public DbSet<Material> Materials { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Material>().ToTable("Material");
        }
    }
}
